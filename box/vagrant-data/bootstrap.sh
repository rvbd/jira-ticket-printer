#!/usr/bin/env bash

#install all the packages
sudo apt-get update

# ### INSTALL WEB SERVER

sudo apt-get -y install apache2
sudo apt-get -y install apache2-utils

# ### PHP SETUP
sudo apt-get -y install php php-mcrypt php-memcached php-curl php-gd php-zip
sudo phpenmod mcrypt

sudo apt-get install -y libapache2-mod-php

sudo a2enmod rewrite
sudo a2enmod headers


#make sure vagrant is in www-data group
sudo chown www-data:www-data /vagrant

sudo rm -rf /var/www/html

sudo ln -fs /vagrant /var/www/html


#lastly setup our Configuration files
echo "Setting up configuration files"

sudo cp /vagrant-data/000-default.conf /etc/apache2/sites-enabled/000-default.conf

cd /vagrant/application/third_party/
sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php
sudo php -r "unlink('composer-setup.php');"

sudo php composer.phar require chobie/jira-api-restclient ^2.0@dev

# lastly reload all necessary services
sudo service apache2 restart
sudo service ssh reload

echo "Configuration files setup done default url is localhost:8088"
echo "Hit http://localhost:8088/setup.php on your browser for 1st time setup"