$(function() {

    // hide the story point mode automatically
    $("#epicPieContainerByStoryPoints").hide();

    // sprintName is output from codeigniter view
    $.get("/ticketprinter/json_sprint_breakdown_epic/" + sprintName).done(function(epicData) {
        $(".error").hide();
    
        // handler for toggling 
        $("#showCountPie").click(function() {
            $("#epicPieContainerByStoryPoints").hide();
            $("#epicPieContainer").show();

            // create label and data array
            var labels = [];
            var data = [];

            for (var epicKey in epicData) {
                labels.push(epicData[epicKey].name);
                data.push(epicData[epicKey].count);
            }

            var ctx = document.getElementById("epicPieChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Epics',
                        data: data,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.3)',
                            'rgba(54, 162, 235, 0.3)',
                            'rgba(255, 206, 86, 0.3)',
                            'rgba(75, 192, 192, 0.3)',
                            'rgba(153, 102, 255, 0.3)',
                            'rgba(255, 159, 64, 0.3)',
                            'rgba(255, 100, 64, 0.3)',
                            'rgba(196, 70, 64, 0.3)',
                            'rgba(255, 178, 249, 0.3)',
                            'rgba(0, 255, 198, 0.3)'
                        ],
                    }]
                },
                options: {
                    pieceLabel: {
                        // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                        render: function(args) {
                            return args.label + " (" + args.percentage + '%)'
                        },
                
                        // precision for percentage, default is 0
                        precision: 0,
                
                        // identifies whether or not labels of value 0 are displayed, default is false
                        showZero: true,
                
                        // font size, default is defaultFontSize
                        fontSize: 16,
                
                        // font color, can be color array for each data or function for dynamic color, default is defaultFontColor
                        fontColor: '#4F5155',
                
                        // font style, default is defaultFontStyle
                        fontStyle: 'bold',
                
                        // font family, default is defaultFontFamily
                        fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                
                        // draw label in arc, default is false
                        arc: false,
                
                        // position to draw label, available value is 'default', 'border' and 'outside'
                        // default is 'default'
                        position: 'border',
                
                        // draw label even it's overlap, default is false
                        overlap: true
                    }
                }
            });
        });
    
        $("#showStoryPointsPie").click(function() {
            $("#epicPieContainerByStoryPoints").show();
            $("#epicPieContainer").hide();
            // EPIC Pie by Story Points
            var labelsStoryPoints = [];
            var dataStoryPoints = [];
            for (var epicKey in epicData) {
                labelsStoryPoints.push(epicData[epicKey].name);
                dataStoryPoints.push(epicData[epicKey].storyPoints);
            }

            var ctxStoryPoints = document.getElementById("epicPieChartStoryPoints").getContext('2d');
            var myChartStoryPoints = new Chart(ctxStoryPoints, {
                type: 'pie',
                data: {
                    labels: labelsStoryPoints,
                    datasets: [{
                        label: 'Epics',
                        data: dataStoryPoints,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.3)',
                            'rgba(54, 162, 235, 0.3)',
                            'rgba(255, 206, 86, 0.3)',
                            'rgba(75, 192, 192, 0.3)',
                            'rgba(153, 102, 255, 0.3)',
                            'rgba(255, 159, 64, 0.3)',
                            'rgba(255, 100, 64, 0.3)',
                            'rgba(196, 70, 64, 0.3)',
                            'rgba(255, 178, 249, 0.3)',
                            'rgba(0, 255, 198, 0.3)'
                        ],
                    }]
                },
                options: {
                    pieceLabel: {
                        // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                        render: function(args) {
                            return args.label + " (" + args.percentage + '%)'
                        },
                
                        // precision for percentage, default is 0
                        precision: 0,
                
                        // identifies whether or not labels of value 0 are displayed, default is false
                        showZero: true,
                
                        // font size, default is defaultFontSize
                        fontSize: 16,
                
                        // font color, can be color array for each data or function for dynamic color, default is defaultFontColor
                        fontColor: '#4F5155',
                
                        // font style, default is defaultFontStyle
                        fontStyle: 'bold',
                
                        // font family, default is defaultFontFamily
                        fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                
                        // draw label in arc, default is false
                        arc: false,
                
                        // position to draw label, available value is 'default', 'border' and 'outside'
                        // default is 'default'
                        position: 'border',
                
                        // draw label even it's overlap, default is false
                        overlap: true
                    }
                }
            });
        });

        // trigger show count pie as default
        $("#showCountPie").click();
    });

    
});