$(function() {
    $("#show_pie").click(function() {
        if ($("#sprint_name").val()) {
            window.location = "/ticketprinter/sprint_breakdown_epic_pie/" + $("#sprint_name").val();
        }
    });
});