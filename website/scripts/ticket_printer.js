$(function() {
	$("#ticket-size").change(function() {
		// remove all the default class first
		$(".ticket-box").removeClass("smaller extra-small large default");
		$(".ticket-box").addClass($(this).val());
	});
});