<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chobie\Jira\Api;
use chobie\Jira\Api\Authentication\Basic;
use chobie\Jira\Issues\Walker;

class Ticketprinter extends CI_Controller {

	/**
	 * Default landing page 
	 */
	public function index()
	{
		$this->load->helper('url');

		$jira_url = $this->session->jira_url;

		if (!$jira_url) {
			$jira_url = $this->config->item('jira_base_url');
		}

		$view_data = array(
			"error_message" => $this->session->error_message,
			"jira_end_point" => $jira_url
		);

		$this->load->view('common/header', $view_data);
		

		if (!$this->session->username || !$this->session->password) {
			$this->load->view('component/loginbox', $view_data);

		} else {

			$this->load->view('component/sprintbox', $view_data);
		}

		
		$this->load->view('common/footer');
	}

	public function login()
	{
		$this->load->helper('url');

		$jira_url = $this->config->item('jira_base_url');

		if ($this->input->post('jira_end_point') && !$jira_url) {
			$jira_url = $this->input->post('jira_end_point');
		}

		if ($this->input->post('username') && $this->input->post('password') && $jira_url) {

			try {
				// try to login
				$api = new \chobie\Jira\Api(
					$jira_url,
					new \chobie\Jira\Api\Authentication\Basic($this->input->post('username'), $this->input->post('password'))
				);

				$api->getResolutions();
			} catch (Exception $e) {
				$this->session->set_flashdata("error_message", $e->getMessage());				
			}

			$this->session->set_userdata(array(
				"username" => $this->input->post('username'),
				"password" => $this->input->post('password'),
				"jira_url" => $jira_url
			));
		}

		redirect('/ticketprinter/');
	}

	public function printlayout() 
	{
		$this->load->helper('url');

		if ($this->input->post("sprintname") || $this->input->post("ticketnumbers")) {

			// determine the filter based on the type of the search, either sprint or ticket numbers

			if ($this->input->post("sprintname")) {
				$jql = 'Sprint = "' . $this->input->post("sprintname") . '" AND type not in (Epic, Sub-task) ORDER BY rank ASC';
			} else {
				// search by ticket number
				$jql = 'key in (' . $this->input->post("ticketnumbers") . ') AND type not in (Epic, Sub-task) ORDER BY rank ASC';
			}

			try {
				// try to login
				$api = new \chobie\Jira\Api(
					$this->session->jira_url,
					new \chobie\Jira\Api\Authentication\Basic($this->session->username, $this->session->password)
				);

				$walker = new \chobie\Jira\Issues\Walker($api);
				$walker->push(
					$jql
				);

				$issues_collection = array();
				
				foreach ( $walker as $issue ) {

					$issue_type = $issue->getIssueType();

					$issues_collection[] = array(
						"description" => $issue->getDescription(),
						"acceptance_criteria" => $issue->get('Acceptance Criteria'),
						"summary" => $issue->getSummary(),
						"key" => $issue->getKey(),
						"points" => $issue->get('Story Points'),
						"type" => $issue_type['name'],
						"type_style" => "type-" . str_replace(' ', '-', strtolower($issue_type['name']))
					);
				}

				$this->load->view('common/header');
				$this->load->view('ticket_printer', array(
					"tickets" => $issues_collection,
					"sprint" => $this->input->post("sprintname"),
					"jira_end_point" => $this->session->jira_url
				));
				$this->load->view('common/footer');

			} catch (Exception $e) {
				$this->session->set_flashdata("error_message", $e->getMessage());
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('password');
				$this->session->unset_userdata('jira_url');
				redirect('/');
			}

			
		} else {
			$this->session->set_flashdata("error_message", "You need to enter the sprint name");
			redirect('/');
		}
	}

	public function sprint_breakdown_epic_pie($sprint_name) 
	{
		// make sure to decode URL
		$sprint_name = urldecode($sprint_name);
		
		$this->load->view('common/header');
		$this->load->view('epic_pie', array(
			"sprint_name" => $sprint_name,
			"jira_end_point" => $this->session->jira_url
		));
		$this->load->view('common/footer');
	}

	public function json_sprint_breakdown_epic($sprint_name) 
	{
		// make sure to decode URL
		$sprint_name = urldecode($sprint_name);

		$this->load->helper('url');
		if ($sprint_name) {
			$jql = 'Sprint = "' . $sprint_name . '" AND type not in (Epic, Sub-task) ORDER BY rank ASC';

			try {
				// try to login
				$api = new \chobie\Jira\Api(
					$this->session->jira_url,
					new \chobie\Jira\Api\Authentication\Basic($this->session->username, $this->session->password)
				);

				$walker = new \chobie\Jira\Issues\Walker($api);
				$walker->push(
					$jql
				);

				$issues_epic_collection = array();
				
				foreach ( $walker as $issue ) {

					$issue_type = $issue->getIssueType();

					$issue_epic_key = $issue->get('Epic Link');
									
					if ($issue_epic_key) {
						if (!array_key_exists($issue_epic_key, $issues_epic_collection)) {
							// create the structure
							$issues_epic_collection[$issue_epic_key] = array(
								"name" => "",
								"count" => 0,
								"storyPoints" => 0
							);
						}

						$issues_epic_collection[$issue_epic_key]["count"]++;
						$issues_epic_collection[$issue_epic_key]["storyPoints"] += $issue->get('Story Points');
					}
				}
				
				// decorate epic collection with the names
				$epic_jql = "Key in(" . trim(implode(",", array_keys($issues_epic_collection)), ",") . ")";
				
				$epic_walker = new \chobie\Jira\Issues\Walker($api);
				$epic_walker->push(
				 	$epic_jql
				);
				foreach ($epic_walker as $epic) {
					$epic_key = $epic->getKey();
					if ($issues_epic_collection[$epic_key]) {
						$issues_epic_collection[$epic_key]["name"] = $epic->get("Epic Name");
					}
				}

				$this->output->set_content_type('application/json');
				echo json_encode($issues_epic_collection);
				return true;

			} catch (Exception $e) {
				$this->session->set_flashdata("error_message", $e->getMessage());
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('password');
				$this->session->unset_userdata('jira_url');
				redirect('/');
			}
		}
	}

	public function end() 
	{
		$this->load->helper('url');

		$this->session->unset_userdata('username');
		$this->session->unset_userdata('password');
		$this->session->unset_userdata('jira_url');

		redirect('/');
	}
}
