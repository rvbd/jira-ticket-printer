<h1>Tickets in Sprint <?=$sprint?></h1>
<em class="jira-url">JIRA URL: <?=$jira_end_point?></em>
<div class="print-menu">
	<a href="javascript:window.print()">Print</a> | <a href="/ticketprinter/">Back to Menu</a>
	| <strong>Size:</strong> <select name="ticket-size" id="ticket-size">
		<option value="default">Default size, 4 tickets per A4 page</option>
		<option value="smaller">Smaller, 6 tickets per A4 page</options>
		<option value="extra-small">Extra small, 12 tickets per A4</option>
		<option value="large">Large, 3 tickets per A4</option>
	</select>
</div>
<?php
foreach ($tickets as $ticket) {
?>
	<div class="ticket-box <?=$ticket['type_style']?>"> 
		<div class="content">
			<span class="ticket-key"><?=$ticket["key"]?><span class="ticket-type">[<?=$ticket["type"]?>]</span></span>		
			<span class="ticket-points"><?=$ticket["points"]?></span>
			<span class="ticket-summary"><div class="content"><?=$ticket["summary"]?></div></span>
			<div class="ticket-description">
				<div class="content">
					<?php if ($ticket["acceptance_criteria"]) { ?>
						<?=nl2br($ticket["acceptance_criteria"])?>
					<?php } if ($ticket["description"]) { ?>
						<hr />
						<?=$ticket["description"]?>
					<?php } ?>
				</div>
			</div>
			<div class="ticket-additional"></div>
		</div>
	</div>
<?php
}
?>

<script src="/scripts/ticket_printer.js"></script>