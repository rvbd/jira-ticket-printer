<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Simple JIRA Ticket Printer</title>
	<link rel="stylesheet" type="text/css" href="/style.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

<div id="container">
	<?php
	if (isset($error_message) && $error_message) { ?>
			<p class="error"><?=$error_message?></p>
	<?php } ?>

	<h1>Simple JIRA Ticket Printer</h1>