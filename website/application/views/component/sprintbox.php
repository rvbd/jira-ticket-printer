<em>JIRA URL: <?=$jira_end_point?></em>
<div class="search-box-container">
	<h2>Print by Sprint</h2>
	<form method="post" action="/ticketprinter/printlayout/">
		<label for="sprintname">Sprint Name: </label>
		<input type="text" name="sprintname" id="sprintname" />

		<input type="submit" value="Show me the cards!" />
	</form>
</div>

<div class="search-box-container">

	<h2>Print by Tickets</h2>
	<p>Enter ticket numbers separated by comma</p>
	<form method="post" action="/ticketprinter/printlayout/">
		<label for="ticketnumbers">Ticket numbers: </label>
		<input type="text" name="ticketnumbers" id="ticketnumbers" />

		<input type="submit" value="Show me the cards!" />
	</form>
</div>

<div class="search-box-container">
	<h2>Epic Pie (A4 printout)</h2>
	<label for="ticketnumbers">Sprint Name: </label>
	<input type="text" name="sprint_name" id="sprint_name" />

	<input type="submit" value="Show me the pie!" id="show_pie" />
</div>

<div class="search-box-container">
	<h2>End Session</h2>
	<?php if (!$jira_end_point) { // if jira end point not configure allow user to override ?>
		<p>End this session to use different url or user.</p> 
	<?php } else { ?>
		<p>End this session to use different user.</p> 
	<?php } ?>
	<form method="post" action="/ticketprinter/end/">
		<input type="submit" value="Quit" />
	</form>
</div>

<script src="/scripts/sprintbox.js"></script>