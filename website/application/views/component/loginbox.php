<p>Enter your JIRA login to begin</p>
<form method="post" action="/ticketprinter/login/">
	<label for="username">Username</label>
	<input type="text" name="username" id="username" />

	<label for="password">Password</label>
	<input type="password" name="password" id="password" />

	<?php if (!$jira_end_point) { // if jira end point not configure allow user to override ?>
	<p>
		<label for="jira_end_point">Jira URL (eg. https://jira.atlassian.com)</label>
		<input type="text" name="jira_end_point" id="jira_end_point" />
	<p>
	<?php } ?>

	<input type="submit" value="login" />
	<input type="hidden" value="login" name="action" /> 
</form>