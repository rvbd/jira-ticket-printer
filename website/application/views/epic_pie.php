<h1 class="show-in-print">Epic Breakdown of Sprint <?=$sprint_name?></h1>
<em class="jira-url">JIRA URL: <?=$jira_end_point?></em>
<h2><a href="javascript:window.print()">Print</a> | 
<a href="/ticketprinter/">Back to Menu</a> |
<a href="#" id="showCountPie">Show Distribution by Number of Tickets</a> |
<a href="#" id="showStoryPointsPie">Show Distribution by Story Points </a>
</h2>
<script>
	var sprintName = "<?=$sprint_name?>";
</script>

<p class="error"><em>Loading...</em></p>
<div class="epicPieContainer" id="epicPieContainer">
	<h1 class="show-in-print">Number of Tickets Distribution</h2>
	<canvas id="epicPieChart" width="1024" height="768"></canvas>
</div>

<div class="epicPieContainer" id="epicPieContainerByStoryPoints">
	<h1 class="show-in-print">Story Points Distribution</h2>
	<canvas id="epicPieChartStoryPoints" width="1024" height="768"></canvas>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
<script src="/scripts/piecelabel.js"></script>
<script src="/scripts/epic_pie.js"></script>